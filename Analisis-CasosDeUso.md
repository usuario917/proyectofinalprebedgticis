# Análisis - Casos de Uso
1. Elaboren el Diagrama de casos de uso de toda la funcionalidad del sistema.
2. Realicen la especificación de cada caso de uso del diagrama que generaron en el punto anterior. Utilicen la plantilla que contiene el archivo Plantilla_CasoDeUso.docx.
3. Deberán generar un archivo .zip con el diagrama de casos de uso y las especificaciones de cada caso de uso y subirlo a la plataforma.