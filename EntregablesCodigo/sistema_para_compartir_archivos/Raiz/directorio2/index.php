<?php
	//Conexión con la BD
	$host = "localhost";
	$db = "sistema_para_compartir_archivos";
	$user = "root";
	$password = "";
	
	$dsn = "mysql:host=$host;dbname=$db";
	$dbh = new PDO($dsn, $user, $password);
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	//Obtención de el arreglo de objetos
	$objetos=array();
  try {
    // FETCH_OBJ
    $stmt = $dbh->prepare("SELECT * FROM Objeto");
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_OBJ);
    if (!empty($result)) {
      foreach($result as $row) {
        $stmt2=$dbh->prepare("
        WITH RECURSIVE cte (idObjeto,idDirPadre,nombre)
        AS (
          SELECT '".$row->idObjeto."','".$row->idDirPadre."','".$row->nombre."'
          UNION ALL
          SELECT o.idObjeto,o.idDirPadre,o.nombre 
          FROM Objeto o 
          join cte e 
          on e.idDirPadre=o.idObjeto  
        )
        SELECT * FROM cte
        ");
        $stmt2->execute();
        $result2=$stmt2->fetchAll(PDO::FETCH_OBJ);
        $ruta="";
        foreach($result2 as $row2){
          $ruta=$row2->nombre."/".$ruta;
        }
        $objetos[$row->idObjeto]=substr($ruta,0,-1);
      }
    }
  } catch (Exception $e) {
      // Cualquier error lo imprimimos
      echo $e->getMessage();
  } 
	
	// Start the session
	session_start();
	//Las siguientes líneas son para obtener la ruta relativa sobre el directorio raíz
	$rutaDirectorio = substr(__FILE__,$_SESSION['longRaiz']);
	$rutaDirectorio = substr($rutaDirectorio,0,-strlen("/index.php"));
	
	if (!isset($_SESSION["idUsuario"])) { //No ha iniciado sesión
		header("Location: http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/sistema_para_compartir_archivos/Vista/login.php");
	} else if (isset($_SESSION["idUsuario"])) { //Ya ha iniciado sesión
		?>
		<!DOCTYPE html>
		<html lang="es">
			<head>
				<title>Explorador</title>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<?php
					echo "<link rel=\"stylesheet\" type=\"text/css\"".
					"href=\"http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/".
					"sistema_para_compartir_archivos/Vista/css/index.css\">";
				?>
			</head>
			<body>
				<header>
					<nav>
						<a href="#">Crear usuario</a>
						<!--<a href="crearDirectorio.php" onclick="crearCarpeta()">Crear directorio</a>-->
						<a onclick="crearCarpeta()">Crear directorio</a>
						<?php
							echo "<a href=\"http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/".
							"sistema_para_compartir_archivos/Controlador/AP1.php\">Gestionar permisos</a>";
						?>
						<a href="#">Subir archivo</a>
						<?php
							echo "<a href=\"http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/".
							"sistema_para_compartir_archivos/Controlador/logout.php\">Salir</a>";
						?>
						<div class="animation start-home"></div>
					</nav>
				</header>
				<main>
					<h1>Contenido de la carpeta</h1>
					<?php
						//Obtiene el id del directorio actual
						$rutaDirectorio=str_replace("\\", "/", $rutaDirectorio);
						$idDirectorioActual = array_search($rutaDirectorio,$objetos);

						//Genera consulta para obtener los objetos del directorio actual
						try {
							// FETCH_OBJ
							$sql="
							SELECT o.idObjeto, o.nombre
							FROM Objeto o
							JOIN Permisos p
							ON o.idObjeto=p.idObjeto
							JOIN Usuario u
							ON u.idUsuario=p.idUsuario
							WHERE u.idUsuario=".$_SESSION["idUsuario"]."
							AND p.lectura='1'
							AND o.idDirPadre=".$idDirectorioActual;
							$stmt = $dbh->prepare($sql);

							$stmt->execute();
							$result = $stmt->fetchAll(PDO::FETCH_OBJ);
							/* if (!empty($result)) {
								foreach($result as $row) {
									echo $row->idObjeto." ".$row->nombre."<br>";
								}
							} */
						} catch (Exception $e) {
								// Cualquier error lo imprimimos
								echo $e->getMessage();
						} finally {
								// Cerramos la conexion a la base
								$dbh = null;
						}


						$manejador=opendir(".");
						$nombre_archivo=array();
						//Ingorando la carpeta actual (punto) y el su propio index.php
						$elemento_ignorado = array ('.','index.php');
						//$icono_elemento = "../imagenes/not_found.png";
						$icono_elemento = "http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/".
						"sistema_para_compartir_archivos/Vista/img/not_found.png";
						$contenido = "";
						/* while ($archivo=readdir($manejador)) {
							$nombre_archivo[]=$archivo;
						}
						sort($nombre_archivo); */
						foreach($result as $row) {
							$archivo=$row->nombre;
							if (is_dir($archivo)) { //Es un directorio
								$icono_elemento = "http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/".
								"sistema_para_compartir_archivos/Vista/img/folder.png";
							} else { //Es un archivo
								$icono_elemento = "http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/".
								"sistema_para_compartir_archivos/Vista/img/file.png";
							}
							if (!in_array($archivo,$elemento_ignorado)) {
								if($archivo=="..") {
									$contenido .=
										"<div>".
											"<a class=\"col-lg-1\" href=\"".$archivo."\">".
												"<img id=fle src=\"http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/".
												"sistema_para_compartir_archivos/Vista/img/volver.png\" alt=\"Regresar\" title=\"Regresar\">".
											"</a>".
											"<a class=\"col-lg-1\" href=\"".$archivo."\">Regresar</a>".
										"</div>\n";
								}
								else
									$contenido .=
										"<div>".
											"<a class=\"col-lg-1\" href=\"".$archivo."\">".
												"<img src=\"$icono_elemento\" alt=\"".$archivo."\" title=\"".$archivo."\">".
											"</a>".
											"<a class=\"col-lg-1\" href=\"".$archivo."\">".$archivo."</a>".
										"</div>\n";
							}
						}
						closedir($manejador);
						echo $contenido . "\n";
					?>
				</main>
				<!-- Optional JavaScript -->
				<?php
				$rutaActual = str_replace("index.php", "", __FILE__);
				echo "<script>";
					echo "function crearCarpeta() {";
						//var carpetaACrear = prompt("Cual es el nombre del directorio a crear?");
                    echo 'url="http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/sistema_para_compartir_archivos/Controlador/crearDirectorio.php?carpetaACrear="+prompt("Cual es el nombre del directorio a crear?")+"&rutaActual='.$rutaActual.'";';
						//url="http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/sistema_para_compartir_archivos/Controlador/crearDirectorio.php?carpetaACrear="+carpetaACrear;
						echo "location.href=url;";
					echo "}";
				echo "</script>";
				?>
				<!-- jQuery first, then Popper.js, then Bootstrap JS -->
				<?php
					echo "<script src=\"http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/".
					"sistema_para_compartir_archivos/Vista/js/jquery-3.4.1.min.js\"></script>";
				?>
				<?php
					echo "<script src=\"http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/".
					"sistema_para_compartir_archivos/Vista/js/popper.min.js\"></script>";
				?>
				<?php
					echo "<script src=\"http://localhost/ProyectoFinalPrebeDgticIS/EntregablesCodigo/".
					"sistema_para_compartir_archivos/Vista/js/bootstrap.min.js\"></script>";
				?>
			</body>
		</html>
		<?php
	}
?>
