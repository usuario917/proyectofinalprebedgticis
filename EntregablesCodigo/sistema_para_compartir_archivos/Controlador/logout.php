<?php
	// Start the session
	session_start();
	session_destroy();
	try {
		// Preparamos la conexion a la base de datos
		require_once('./conn.php');
	} catch (Exception $e) {
		// Cualquier error lo imprimimos
		echo $e->getMessage();
	} finally {
		// Cerramos la conexion a la base
		$dbh = null;
	}
	header('Location: ../Vista/login.php');
?>