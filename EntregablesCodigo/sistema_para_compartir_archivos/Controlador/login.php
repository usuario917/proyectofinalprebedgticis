<?php
	// Start the session
	session_start();
	if (isset($_SESSION["idUsuario"])) { //Ya ha iniciado sesión
		header('Location: ../Raiz/index.php');
	} else if (!isset($_SESSION["idUsuario"])) { //No ha iniciado sesión
		if((!isset($_POST["email"])) || (!isset($_POST["contrasena"]))) { //No ha enviado datos
			header('Location: ../Vista/login.php');
		} else if ((isset($_POST["email"])) && (isset($_POST["contrasena"]))) { //Ya envió datos
			try {
				// Preparamos la conexion a la base de datos
				require_once("conn.php");
				// consulta de datos
				// FETCH_ASSOC
				$stmt = $dbh->prepare("SELECT * FROM Usuario");
				// Especificamos el fetch mode antes de llamar a fetch()
				$stmt->setFetchMode(PDO::FETCH_ASSOC);
				// Ejecutamos
				$stmt->execute();
				// Comparamos los resultados
				while ($row = $stmt->fetch()) {
					if(($row["email"] === $_POST["email"]) && ($row["contrasena"] === $_POST["contrasena"])) { //Ya esta la Sesión :DDDDD
						$_SESSION["idUsuario"] = $row["idUsuario"];
						// Cerramos la conexion a la base
						$dbh = null;
						header('Location: ../Raiz/index.php');
					} else {
						header('Location: ../Vista/login.php');
					}
				}
			} catch (Exception $e) {
				// Cualquier error lo imprimimos
				echo $e->getMessage();
			} finally {
				// Cerramos la conexion a la base
				$dbh = null;
			}
		}
	}
?>