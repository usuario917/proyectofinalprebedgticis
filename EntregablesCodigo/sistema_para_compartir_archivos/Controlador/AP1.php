<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Ver archivosAP</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maxium-scale=1.0, minium-scale=1.0">
  <link rel="stylesheet" type="text/css" href="../Vista/css/AP1.css">
</head>

<body>
  <img id="flecha" src=../Vista/img/volver.png onclick="location='../Raiz/index.php'">
  <div class="container">
    <div class="row">
      <div class="col-sm-14 col-lg-12">
        <h2 class="page-header text-center titulo">Mis directorios y archivos </h2>
      </div>
    </div>
  </div>
  <div class="container letras">
    <div class="row">
      <div class="col-sm-14 col-lg-12">
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">Nombre</th>
              <th ruta="col">Ruta</th>
              <th scope="col">Opciones</th>
            </tr>
          </thead>
          <tbody>
            <?php
              session_start();
              require_once('./conn.php');
              try {
                // FETCH_OBJ
                $sql="SELECT * 
                FROM Objeto o
                WHERE o.idPropietario=".$_SESSION["idUsuario"];
                $stmt = $dbh->prepare($sql);
                $stmt->execute();
                $result = $stmt->fetchAll(PDO::FETCH_OBJ);
                if (!empty($result)) {
                  foreach($result as $row) {
                    //Obtiene la ruta de objeto
                    $stmt2=$dbh->prepare("
                    WITH RECURSIVE cte (idObjeto,idDirPadre,nombre)
                    AS (
                        SELECT '".$row->idObjeto."','".$row->idDirPadre."','".$row->nombre."'
                        UNION ALL
                        SELECT o.idObjeto,o.idDirPadre,o.nombre 
                        FROM Objeto o 
                        join cte e 
                        on e.idDirPadre=o.idObjeto  
                    )
                    SELECT * FROM cte
                    ");
                    $stmt2->execute();
                    $result2=$stmt2->fetchAll(PDO::FETCH_OBJ);
                    $ruta="";
                    foreach($result2 as $row2){
                        $ruta=$row2->nombre."/".$ruta;
                    }
                    $ruta=substr($ruta,0,-1);

                    //Imprime el renglón con el nombre y la ruta
                    echo "
                      <form class=\"form\"
                        action=\"http://localhost/ProyectoFinalPrebeDgticIS/"
                        ."EntregablesCodigo/sistema_para_compartir_archivos/"
                        ."Controlador/asignarPermisos.php\"
                      method=\"post\">
                      <tr>
                        <td>{$row->nombre}</td>
                        <td>{$ruta}</td>
                        <td>
                        <input type=\"hidden\" name=\"idObjeto\"
                            value=\"".$row->idObjeto."\">
                        <input type=\"submit\" name=\"AP\"
                            class=\"bAP\" value=\"Asignar permisos\"/></td>
                      </tr> 
                      </form>
                      ";
                    }
                } else {
                  echo "<tr><td colspan='7'>No hay datos para mostrar</td></tr>";
                }
              } catch (Exception $e) {
                  // Cualquier error lo imprimimos
                  echo $e->getMessage();
              } finally {
                  // Cerramos la conexion a la base
                  $dbh = null;
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>

</html>