<?php
$host = "localhost";
$db = "sistema_para_compartir_archivos";
$user = "root";
$password = "";

$dsn = "mysql:host=$host;dbname=$db";
$dbh = new PDO($dsn, $user, $password);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
