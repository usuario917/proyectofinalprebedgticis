
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Crear usuarios</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maxium-scale=1.0, minium-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../Vista/css/CU.css">
</head>
<body>
<img id="flecha" src=../Vista/img/volver.png onclick="location='../Raiz/index.php'">
	<div class="container">
		<div class="row">
			<div class="col-sm-14 col-lg-12">
			<h2 class="page-header text-center titulo">Crear cuenta de usuario</h2>
			</div>
		</div>	
</div>
<div class="container letras">
	<div class="row">
		<div class="col-sm-14 col-lg-12">
			<form class="form" action="recibir.php" method="POST">
<div class="form-group">
	<label for="email" class="col-sm-12 control-label">Correo electrónico</label>
	<div class="col-sm-14"><input type="email" class="form-control" name="email" required></div>
</div>
<div class="form-group">
	<label for="al_nombre" class="col-sm-12 control-label">Nombre</label>
	<div class="col-sm-14"><input type="texto" class="form-control"  name="al_nombre" required></div>
</div>
<div class="form-group">
	<label for="al_apellido1" class="col-sm-12 control-label">Apellido1</label>
	<div class="col-sm-14"><input type="texto" class="form-control" name="al_apellido1" required></div>
</div>
<div class="form-group">
	<label for="al_apellido2" class="col-sm-12 control-label">Apellido2</label>
	<div class="col-sm-14"><input type="texto" class="form-control" name="al_apellido2"required> </div>
</div>
<div class="form-group">
	<label for="contraseña" class="col-sm-12 control-label">Contraseña</label>
	<div class="col-sm-14"><input type="password" class="form-control"  name="contraseña"></div>
</div>
	<label class="form-label">Permisos sobre el directorio raíz</label> <br>
	<label class="form-radio">
		<input type="radio" type="char" name="lec" value="lec" checked>
			<i class="form-icon"></i>Lectura
	</label><br>
	<label class="form-radio">
	<input type="radio" name="esc" value="es">
		<i class="form-icon"></i> Escritura
	</label><br>
	<label class="form-radio">
	<input type="radio" name="eli" value="eli">
		<i class="form-icon"></i> Eliminación
	</label><br>
    <label class="form-label">¿Es administrador?</label> <br>
	<label class="form-radio">
		<input type="radio" type="char" name="si" value="si" checked>
			<i class="form-icon"></i>Si
	</label><br>
	<label class="form-radio">
	<input type="radio" name="no" value="no">
		<i class="form-icon"></i> No
	</label><br>
<div class="form-group">
<div style="text-align:right">
 	<div class="col-sm-offset-14 col-sm-12 text-center">
	<button type="submit" name="enviar" class="btn btn-default button">Aceptar</button>
	</div>
</div>
<br>
<br/>
</div>
</form>
	</div>
		</div>
		</div>
		
	</body>
</html>
