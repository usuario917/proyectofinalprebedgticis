<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Asignar permisos</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.min.css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../Vista/css/AP.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
  </head>
  <body>
    <img id="flecha" src=../Vista/img/volver.png onclick="location='../Controlador/AP1.php'">
    <h3>Asignar permisos</h3>
    <div class="wrap">
      <form class="formulario" action="mensaje.php" method="POST">
        <label class="form-label">Nombre completo del colaborador: </label>
        <select class="selectpicker"
          data-show-subtext="true" 
          data-style="form-control"           
          data-live-search="true"
          name='idUsuario'
          data-style="form-control" 
          data-live-search="true" 
          title="-- Ingresa el nombre --">
          <?php
            session_start();
            // Conexion
            function connect(){
              return new mysqli("127.0.0.1","root","","sistema_para_compartir_archivos");
            }
            $con = connect();
            if (!$con->set_charset("utf8")) {//asignamos la codificación comprobando que no falle
              die("Error cargando el conjunto de caracteres utf8");
            }
            $consulta = "SELECT * FROM Usuario
            WHERE idUsuario <>".$_SESSION["idUsuario"];
            $resultado = mysqli_query($con , $consulta);
            $contador=0;
            while($misdatos = mysqli_fetch_assoc($resultado)) { $contador++;?>
              <option data-subtext="<?php echo  $misdatos ["email"]; ?>"
                value="<?php echo $misdatos["idUsuario"]?>">
              <?php echo $misdatos["nombre"]; ?>
              <?php echo $misdatos["apellido1"]; ?>
              <?php echo $misdatos["apellido2"]; ?></option>
              <?php
            }
            ?>
        </select>
        <br>
        <div class="checkbox">
          <h2>Permisos:</h2>
          <input type="checkbox" id="lec"  value="Lectura" name="opc[]">
          <label for="lec">Lectura</label>
          <input type="checkbox" id="esc" value="Escritura" name="opc[]">
          <label for="esc">Escritura</label>
          <input type="checkbox" id="elim" value="Eliminación" name="opc[]">
          <label for="elim">Eliminación</label>
          <input type="hidden" name="idObjeto"
            value="<?php echo $_POST["idObjeto"];?>">
        </div>
        <button type="submit" name="guardar">Guardar</button></td>
      </form>
    </div>
  </body>
</html>




