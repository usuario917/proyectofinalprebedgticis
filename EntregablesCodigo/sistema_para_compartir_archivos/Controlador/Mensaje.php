<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Mensaje</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>
    <link rel="stylesheet" type="text/css" href="../Vista/css/mensaje.css">
  </head>
  <body>
    <h3>Mensaje</h3>
    <div class="wrap">
      <form action="AP1.php" method="post" class="formulario">
      <?php
      require_once('./conn.php');

      $permisos="";
      $arrPermisos=array();
      //El arreglo por defecto contiene todos los permisos  
      $arregloDefecto=array("lectura","escritura","eliminacion");
      if(isset($_POST['opc'])){
        //el siguiente for tiene como objetivo generar una cadena que indique
        //los permisos asignados para impirmirlos en el mensaje
        //también pasa todos los valores a lower case y elimina tildes
        foreach($_POST['opc'] as &$value){
            $permisos.=$value.", ";
            $value=str_replace("ó","o",$value);
            $value=strtolower($value);
        }
        $arrPermisos=$_POST['opc'];
      }

      //dependiendo de si encuentra o no el permiso, significa que se 
      //asigna a 0 o a 1
      foreach($arregloDefecto as $val){
        if(in_array($val,$arrPermisos)){
          //echo "1 ".$val;
          $sql = "UPDATE Permisos SET ".$val." = '1'
                  WHERE idObjeto=".$_POST["idObjeto"]."
                  AND idUsuario=".$_POST["idUsuario"];
          $stmt = $dbh->prepare($sql);
          $stmt->execute();
        }
        else{
          //echo "0 ".$val;
          $sql = "UPDATE Permisos SET ".$val." = '0'
                  WHERE idObjeto=".$_POST["idObjeto"]."
                  AND idUsuario=".$_POST["idUsuario"];
          $stmt = $dbh->prepare($sql);
          $stmt->execute();
        }
      }
      $sql="SELECT CONCAT(
        IFNULL(nombre,\"\"),\" \",IFNULL(apellido1,\"\"),\" \",
        IFNULL(apellido2,\"\"))
      AS nombre_completo
      FROM Usuario
      WHERE idUsuario=:idUsuario";
      $stmt = $dbh->prepare($sql);
      $stmt->bindValue('idUsuario', $_POST["idUsuario"]);
      $stmt->execute();
      $result=$stmt->fetch(PDO::FETCH_OBJ);
      $nombreUsuario=$result->nombre_completo;
      $dbh=null;

      $permisos=substr($permisos, 0,-2);
      //echo $permisos;
      $mensaje ="Se asignarón los permisos de " .$permisos." a ".$nombreUsuario 
        ." ¿Desea guardar los cambios?";

      ?>
    <div class="mensaje"> <?php echo $mensaje;?> <br></div><br>

      <button type="submit" id= "guardar" name="guardar" onclick="myFunction()">
        Guardar</button>

      <button type="submit" id="cancelar" name="cancelar">Cancelar</button>
      </form>
    </div>
  </body>

  <script>
      function myFunction() {
      alert("Datos guardados correctamente");
    }
    </script>
</html>