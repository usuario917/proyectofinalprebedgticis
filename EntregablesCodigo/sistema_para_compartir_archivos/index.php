<?php
	// Start the session
	session_start();
	if (isset($_SESSION["idUsuario"])) { //Ya ha iniciado sesión
		header('Location: Raiz/index.php');
	} else if (!isset($_SESSION["idUsuario"])) { //No ha iniciado sesión
		header('Location: Vista/login.php');
	}
?>