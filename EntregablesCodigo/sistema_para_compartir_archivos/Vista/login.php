<?php
	// Start the session
	session_start();
	$longRaiz = strlen(__FILE__)-strlen("Vista\login.php");
	
	if(!isset($_SESSION["longRaiz"])){
		$_SESSION["longRaiz"]=$longRaiz;
	}
	
	if (isset($_SESSION["idUsuario"])) { //Ya ha iniciado sesión
		header('Location: ../Raiz/index.php');
	} else if (!isset($_SESSION["idUsuario"])) { //No ha iniciado sesión
		?>
		<!DOCTYPE html>
		<html lang="es">
			<head>
				<title>Login</title>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
				<link rel="stylesheet" type="text/css" href="css/main.css">
			</head>
			<body>
				<div class="limiter">
					<div class="container-login100">
						<div class="wrap-login100">
							<div class="login100-form-title" style="background-image: url(img/bg-01.jpg);">
								<span class="login100-form-title-1">
									Login
									<h6 class= "sub text-center">Sistema para compartir archivos</h6>
								</span>
							</div>
							<form class="login100-form validate-form" action="../Controlador/login.php" method="POST">
								<div class="wrap-input100 validate-input m-b-26" data-validate="Correo electrónico es requerido">
									<span class="label-input100">Correo electrónico</span>
									<input class="input100" type="text" name="email" placeholder="ejemplo@dominio.ext">
									<span class="focus-input100"></span>
								</div>
								<div class="wrap-input100 validate-input m-b-18" data-validate = "Contraseña es requerida">
									<span class="label-input100">Contraseña</span>
									<input class="input100" type="password" name="contrasena" placeholder="⏺⏺⏺⏺⏺⏺⏺⏺⏺⏺⏺">
									<span class="focus-input100"></span>
								</div>
									<div>
										<a href="#" class="txt1">¿Olvidaste tu contraseña?</a>
									</div>
								<div class="container-login100-form-btn">
									<button class="login100-form-btn" type="submit" name="entrar">Iniciar sesión</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<script src="js/jquery-3.4.1.min.js"></script>
				<script src="js/main.js"></script>
			</body>
		</html>
		<?php
	}
?>
