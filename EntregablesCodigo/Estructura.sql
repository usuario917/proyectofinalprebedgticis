DROP DATABASE IF EXISTS sistema_para_compartir_archivos;
CREATE DATABASE sistema_para_compartir_archivos;

USE sistema_para_compartir_archivos;

/*drop table if EXISTS Usuario;
drop table if EXISTS Permisos;
drop table if EXISTS Objeto;*/

CREATE TABLE Usuario (
	idUsuario INTEGER unsigned NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(36) NULL,
	apellido1 VARCHAR(36) NULL,
	apellido2 VARCHAR(36) NULL,
	email VARCHAR(50) UNIQUE NOT NULL,
	contrasena VARCHAR(30) NOT NULL,
	creadorUsuarios CHARACTER(1) NOT NULL,
	PRIMARY KEY (idUsuario),
	CONSTRAINT chkcreadorUsuarios CHECK (creadorUsuarios IN ('0','1'))
);

CREATE TABLE Permisos (
	idObjeto INTEGER unsigned NOT NULL,
	idUsuario INTEGER unsigned NOT NULL,
	lectura CHARACTER(1) NOT NULL,
	escritura CHARACTER(1) NOT NULL,
	eliminacion CHARACTER(1) NOT NULL,
	PRIMARY KEY (idObjeto, idUsuario),
	CONSTRAINT chklectura CHECK (lectura IN ('0','1')),
	CONSTRAINT chkescritura CHECK (escritura IN ('0','1')),
	CONSTRAINT chkeliminacion CHECK (eliminacion IN ('0','1'))
);

CREATE TABLE Objeto (
	idObjeto INTEGER unsigned NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(50) NULL,
	idPropietario INTEGER unsigned NOT NULL,
	idDirPadre INTEGER unsigned NULL,
	idTipo INTEGER unsigned NOT NULL,
	PRIMARY KEY (idObjeto)
);

CREATE TABLE TipoObjeto (
	idTipoObjeto INTEGER unsigned NOT NULL AUTO_INCREMENT,
	nombreTipo CHARACTER(10) NOT NULL,
	PRIMARY KEY (idTipoObjeto)
);

ALTER TABLE Permisos ADD CONSTRAINT fkObjetoPermisos /*fk<TablaReferida><TablaForanea>*/
FOREIGN KEY (idObjeto) REFERENCES Objeto (idObjeto) ON DELETE CASCADE;

ALTER TABLE Permisos ADD CONSTRAINT fkUsuarioPermisos /*fk<TablaReferida><TablaForanea>*/
FOREIGN KEY (idUsuario) REFERENCES Usuario (idUsuario) ON DELETE CASCADE;

ALTER TABLE Objeto ADD CONSTRAINT fkUsuarioObjeto /*fk<TablaReferida><TablaForanea>*/
FOREIGN KEY (idPropietario) REFERENCES Usuario (idUsuario) ON DELETE CASCADE;

ALTER TABLE Objeto ADD CONSTRAINT fkObjetoObjeto /*fk<TablaReferida><TablaForanea>*/
FOREIGN KEY (idDirPadre) REFERENCES Objeto (idObjeto) ON DELETE CASCADE;

ALTER TABLE Objeto ADD CONSTRAINT fkTipoObjetoObjeto /*fk<TablaReferida><TablaForanea>*/
FOREIGN KEY (idTipo) REFERENCES TipoObjeto (idTipoObjeto) ON DELETE CASCADE;
