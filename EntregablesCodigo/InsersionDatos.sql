USE sistema_para_compartir_archivos;

delete from Objeto;
delete from TipoObjeto;
delete from Usuario;


ALTER TABLE Usuario AUTO_INCREMENT = 1;
ALTER TABLE TipoObjeto AUTO_INCREMENT = 1;
ALTER TABLE Objeto AUTO_INCREMENT = 1;

INSERT INTO Usuario (nombre,apellido1,apellido2,email,contrasena,creadorUsuarios) 
VALUES
	('Defecto',null,null,'defecto@gmail.com','11111',1),
	('Pepe','Sanchez','De la cruz','PepeSanchezDeLaCruz@gmail.com','12345','0'),
	('Juana','Lopez','Lopez','JuanaLopezLopez@gmail.com','12345','0'), 
	('Pedro','Garcia','Gomez','PedroGarciaGomez@gmail.com','12345','0'),  
	('Pancho','Castillo','De la mancha','PanchoCastilloDeLaMancha@gmail.com','12345','0'), 
	('Erick','Rojas','Perez','ErickRojasPerez@gmail.com','12345','0'); 

INSERT INTO TipoObjeto(nombreTipo) VALUES ('directorio'),('archivo');

INSERT INTO Objeto(nombre, idPropietario, idDirPadre, idTipo)
VALUES
	('Raiz',1,null,1), ('directorio1',2,1,1), ('directorio2',3,1,1), 
	('directorio3',3,3,1), ('archivo1.docx',2,1,2), ('archivo2.xlsx',2,1,2), 
	('archivo3.txt',3,1,2), ('archivo4.pptx',2,2,2), ('archivo5.txt',3,3,2), 
	('archivo6.txt',2,3,2), ('archivo7.rar',2,4,2);

INSERT INTO Permisos(idObjeto, idUsuario, lectura, escritura, eliminacion)
VALUES
	(1,1,'1','1','1'),
	(1,2,'1','1','0'),
	(1,3,'1','1','0'),
	(1,4,'1','1','0'),
	(1,5,'1','1','0'),
	(1,6,'1','1','0'),
	(2,1,'1','1','1'),
	(2,2,'1','1','1'),
	(2,3,'1','0','0'),
	(2,4,'1','0','0'),
	(2,5,'1','0','1'),
	(2,6,'0','0','0'),
	(3,1,'1','1','1'),
	(3,2,'1','1','0'),
	(3,3,'1','1','1'),
	(3,4,'1','0','0'),
	(3,5,'0','0','0'),
	(3,6,'0','0','0'),
	(4,1,'1','1','1'),
	(4,2,'1','1','0'),
	(4,3,'1','1','1'),
	(4,4,'0','0','0'),
	(4,5,'0','0','0'),
	(4,6,'0','0','0'),
	(5,1,'1','1','1'),
	(5,2,'1','1','1'),
	(5,3,'1','1','0'),
	(5,4,'1','1','0'),
	(5,5,'1','1','0'),
	(5,6,'1','1','0'),
	(6,1,'1','1','1'),
	(6,2,'1','1','1'),
	(6,3,'1','1','0'),
	(6,4,'1','1','0'),
	(6,5,'1','1','0'),
	(6,6,'1','1','0'),
	(7,1,'1','1','1'),
	(7,2,'1','1','0'),
	(7,3,'1','1','1'),
	(7,4,'1','1','0'),
	(7,5,'1','1','0'),
	(7,6,'1','1','0'),
	(8,1,'1','1','1'),
	(8,2,'1','1','1'),
	(8,3,'1','1','0'),
	(8,4,'1','1','0'),
	(8,5,'1','1','0'),
	(8,6,'0','0','0'),
	(9,1,'1','1','1'),
	(9,2,'1','1','0'),
	(9,3,'1','1','1'),
	(9,4,'1','1','0'),
	(9,5,'0','0','0'),
	(9,6,'0','0','0'),
	(10,1,'1','1','1'),
	(10,2,'1','1','1'),
	(10,3,'1','1','0'),
	(10,4,'1','1','0'),
	(10,5,'0','0','0'),
	(10,6,'1','1','0'),
	(11,1,'1','1','1'),
	(11,2,'1','1','1'),
	(11,3,'1','1','0'),
	(11,4,'1','1','0'),
	(11,5,'0','0','0'),
	(11,6,'0','0','0');